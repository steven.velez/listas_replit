# Ingresar una lista de numeros, intercambie la posciones de los numeros
# Si la lista es impar deja el ultimo numero intacto. Imprime el resultado

x = input("Ingresa los numeros seguidos de in espacio:\n")
a = list(map(int, x.split()))
# a = [int(s) for s in input().split()]
x = 0
for x in range(0, len(a) - 1, 2):
    valor = a.pop(x)
    a.insert(x + 1, valor)
print(str(a).replace('[', '').replace(']', '').replace(',', ''))
