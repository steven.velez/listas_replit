"""Dados dos enteros (el número de filas my columnas n de m × n 2d lista) y
las siguientes m filas de n enteros, seguidas por dos enteros no negativos i y j
menores que n , intercambie las columnas i y j de la lista 2d e imprime el resultado"""

m, n = [int(s) for s in input().split()]
matriz = [[int(k) for k in input().split()] for i in range(m)]
a, b = [int(s) for s in input().split()]
for t in range(m):
    matriz[t][b], matriz[t][a] = matriz[t][a], matriz[t][b]
    print(*matriz[t], sep=' ')
