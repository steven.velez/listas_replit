# Ingresar una lista de numeros
# Encontrar e imprimir el primer par de elementos el mismo signo. Si no existe ese para imprimir cero

# Read a list of integers:
a = [int(s) for s in input().split()]
# Print a value:
# print(a)
i = 1
for i in range(1, len(a)):
  if a[i] * a[i-1] > 0:
    print(str(a[i - 1]), str(a[i]))
    break
  elif i == len(a)-1:
    print("0")