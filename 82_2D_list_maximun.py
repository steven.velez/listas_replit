"""Dados dos enteros -el número de filas m y las columnas n de la
lista m-n 2d- y las filas m subsiguientes de n enteros, busque el
elemento máximo e imprima su número de fila y número de columna.
Si hay muchos elementos máximos en diferentes filas, informe del
que tiene un número de fila más pequeño. Si hay muchos elementos
máximos en la misma fila, informe del que tenga un número de columna
más pequeño."""

m, n = [int(s) for s in input().split()]
a = [[int(j) for j in input().split()] for i in range(m)]
max_valor, max_i, max_j = a[0][0], 0, 0
for i in range(m):
    for j in range(n):
        if a[i][j] > max_valor:
            max_valor, max_i, max_j = a[i][j], i, j
print(max_i, max_j)
