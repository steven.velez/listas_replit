# Dada una lista de numeros. Imprimir el numero de elementos distintos que tiene

x = input("Ingresa los numeros ordenados con un espacio:\n")
a = list(map(int, x.split()))
# a = [int(s) for s in input().split()]
total = 1

for i in range(1, len(a)):
    if a[i - 1] != a[i]:
        total += 1

print("Hay ",total, "numero")
