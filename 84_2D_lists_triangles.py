"""Dado un número entero n, cree una matriz bidimensional de
tamaño n × n de acuerdo con las siguientes reglas e imprímalo:

En el antidiagonal poner 1.
En las diagonales arriba pone 0.
En las diagonales debajo pone 2."""

n = int(input())
a = [[0] * n for i in range(n)]
for i in range(n):
    for j in range(n):
        if i + j + 1 < n:
            a[i][j] = 0
        elif i + j + 1 == n:
            a[i][j] = 1
        else:
            a[i][j] = 2
for x in a:
    print(*x)
