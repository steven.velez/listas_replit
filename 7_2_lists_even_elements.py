# Ingresar una lista de numeros, imprimir los elementos pares
# No debe usar range

a = [int(s) for s in input("Ingresa los numeros seguidos de un espacio: ").split()]
print("Los numeros pares son:")
for i in a:
    if i % 2 == 0:
        print(i)
