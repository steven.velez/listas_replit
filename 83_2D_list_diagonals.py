#Dado un número entero n , cree una matriz bidimensional de
#tamaño n × n de acuerdo con las siguientes reglas e imprímalo:

var1 = int(input())
a = [[0] * var1 for i in range(var1)]
for i in range(var1):
    for j in range(var1):
        a[i][j] = abs(i - j)
for lin in a:
    print(*lin)
