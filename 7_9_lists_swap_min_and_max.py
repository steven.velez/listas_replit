# Ingresar una lista
# Identificar min y max. Intercambiar sus posiciones(max, min)
# Imprimir la lista resultante

a = [int(s) for s in input("Ingresa los numero seguidos de un espacio:").split()]
max, min = a.index(max(a)), a.index(min(a))
a[max], a[min] = a[min], a[max]

print(a)
